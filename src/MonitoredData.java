import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
    private Date startTime;
    private Date endTime;
    private String activity;
    public static long firstDay;

    public MonitoredData(String startTime, String endTime, String activity) {
        SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            this.startTime = dateFormat.parse(startTime);
            this.endTime = dateFormat.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.activity = activity;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public String getActivity() {
        return activity;
    }

    public long getInterval(){
        return this.endTime.getTime()-this.startTime.getTime();
    }
    public long startDay(){
        return this.startTime.getTime()/86400000;
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", activity='" + activity + '\'' +
                '}';
    }
}
