import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static jdk.nashorn.internal.objects.NativeArray.reduce;

public class Main {
    public static void main(String[] args){
        List<MonitoredData> list;
        //ex1
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get("Activities.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        {
            list = stream.map(line->line.split("\t\t"))
                    .map(line->new MonitoredData(line[0],line[1],line[2]))
                    .collect(toList());
        }
        //set first day



        Optional<Date> fd = list.stream()
                .map(MonitoredData::getStartTime)
                .collect(minBy(Comparator.naturalOrder()));
        MonitoredData.firstDay=fd.get().getTime()/86400000;


        //list.forEach(System.out::println);
        //ex2
        long sum= list.stream()
                .map(MonitoredData::getInterval)
                .reduce(0l,Long::sum);
        System.out.println(sum/86400000);

        //ex3
        Map<String,Integer> map = list.stream()
                .collect(toMap(MonitoredData::getActivity,e->1,Integer::sum));
        map.entrySet().forEach(System.out::println);
        //ex4
        Map<Long,Map<String,Integer>> map2=list.stream()
                .collect(groupingBy(e->e.startDay()-MonitoredData.firstDay,toMap(MonitoredData::getActivity,e->1,Integer::sum)));
        map2.entrySet().forEach(System.out::println);
        //ex5


        IntStream.range(0,list.size())
                .mapToObj(index->String.format("%d:%d",index,list.get(index).getInterval()))
                .forEach(System.out::println);
        //ex6

        Map<String,Long> map3=list.stream()
                .collect(toMap(MonitoredData::getActivity,e->e.getInterval(),Long::sum));
        map3.entrySet().forEach(System.out::println);
        //ex7

        Map<String,Integer> map4=list.stream()
                .collect(toMap(MonitoredData::getActivity,e->1,Integer::sum));
        Map<String,Integer> map5=list.stream()
                .filter(e->e.getInterval()<5000*60)
                .collect(toMap(MonitoredData::getActivity,e->1,Integer::sum));

        List<String> act=list.stream()
                    .map(MonitoredData::getActivity)
                    .filter(e->map4.get(e)*0.9<=map5.getOrDefault(e,0))
                    .distinct()
                    .collect(toList());
        act.forEach(System.out::println);
    }
}
